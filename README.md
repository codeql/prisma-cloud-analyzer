# Prisma Cloud analyzer for gitlab-ci

This analyzer is a wrapper around Twistlock Cli to scan container and parser report to security dashboard
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.


## Usage
```yaml
/analyzer run
```

### Env Variable
| ENV                | Description                                                                                          |
|--------------------|------------------------------------------------------------------------------------------------------|
| TWISTCLI_PATH      | Path to twistcli. Default is twistcli                                                                |
| CONSOLE_URL        | URL for Prisma Cloud Console. Example: --address https://us-west1.cloud.twistlock.com/us-3-123456789 |
| TWISTLOCK_USER     | Access Key ID to access Prisma Cloud                                                                 |
| TWISTLOCK_PASSWORD | Secret Key for the above Access Key                                                                  |
| DOCKER_HOST        | Docker daemon listening address (default: unix:///var/run/docker.sock)                               |
| IMAGE_NAME         | Image to scan                                                                                        |


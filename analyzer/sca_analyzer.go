package analyzer

import (
	"encoding/json"
	"fmt"
	"github.com/fatih/color"
	"github.com/rodaine/table"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"io"
	"os"
	"os/exec"
	"prisma-cloud-analyzer/metadata"
	"prisma-cloud-analyzer/safebug"
	"strings"
)

func NewScaAnalyzer(c *cli.Context) Analyzer {
	return &ScaAnalyzer{
		BaseAnalyzer: BaseAnalyzer{
			TargetDir:    c.String(FlagTargetDir),
			TwistCliPath: c.String(FlagTwistCliPath),
			ConsoleUrl:   c.String(FlagUrl),
			User:         c.String(FlagUser),
			Password:     c.String(FlagPass),
			OutputFile:   c.String(FlagOutput),
			ShowDetail:   c.Bool(FlagDetails),
			GitlabCI:     NewEnvGitlabCI(),
			SafebugURL:   c.String(flagSafebugUrl),
			SafebugToken: c.String(flagSafebugToken),
		},
		RepoName: c.String(FlagRepoName),
	}
}

type ScaAnalyzer struct {
	BaseAnalyzer
	RepoName string
}

func (this *ScaAnalyzer) Analyze() (io.ReadCloser, error) {
	args := []string{
		"coderepo", "scan",
		"--address", this.ConsoleUrl,
		"--user", this.User,
		"--password", this.Password,
		"--output-file", this.OutputFile,
		"--repository", this.RepoName,
	}
	if this.ShowDetail {
		args = append(args, "--details")
	}
	args = append(args, this.TargetDir)
	cmd := exec.Command(this.TwistCliPath, args...)
	cmd.Env = os.Environ()
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		log.Panic(err.Error())
	}
	go printStdout(stdout)
	go printStderr(stderr)
	err = cmd.Wait()
	return os.Open(this.OutputFile)
}

func (this *ScaAnalyzer) Convert(reader io.Reader) (*report.Report, error) {
	prePath := this.TargetDir
	buf, err := io.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	var scanner = report.Scanner{
		ID:   metadata.AnalyzerID,
		Name: metadata.AnalyzerName,
	}
	var rep SCAReport
	_ = json.Unmarshal(buf, &rep)
	// push report to safebug
	if this.SafebugURL != "" && this.SafebugToken != "" {
		safebugClient := safebug.NewSafeBug(this.SafebugURL, this.SafebugToken)
		safebugClient.RefreshToken()
		// create or update project gitlab
		projectId := safebugClient.CreateOrUpdateProjectGitlab(safebug.CreateUpdateProjectGitlabRequest{
			Name:             this.GitlabCI.ProjectPath,
			ProjectGitlabUrl: this.GitlabCI.ProjectURL,
			ProjectGitlabId:  this.GitlabCI.ProjectID,
		})
		mDep := make(map[string]int)
		dependencies := safebugClient.GetDependenciesProjectGitlab(this.GitlabCI.ProjectID)
		for _, item := range dependencies {
			key := fmt.Sprintf("%s@%s:%s", item.Name, item.Version, item.Location)
			mDep[key] = item.Id
		}
		for _, pack := range rep.Packages {
			path := strings.ReplaceAll(pack.Path, prePath, "")
			path = strings.Trim(path, "/")
			key := fmt.Sprintf("%s@%s:%s", pack.Name, pack.Version, path)
			_, ok := mDep[key]
			// new dependency
			if ok == false {
				log.Info(fmt.Sprintf("New dependency %s@%s at %s", pack.Name, pack.Version, path))
				safebugClient.CreateDependencyProjectGitlab(this.GitlabCI.ProjectID, safebug.CreateDependencyProjectRequest{
					Name:     pack.Name,
					Version:  pack.Version,
					Location: path,
				})
			} else {
				delete(mDep, key)
			}
		}
		// delete old dependency
		for key, depId := range mDep {
			log.Info("Remove old dependency: " + key)
			safebugClient.DeleteDependencyProjectGitlab(this.GitlabCI.ProjectID, depId)
		}
		// update cve
		scaConfig := safebugClient.GetScaThresholdConfig(this.GitlabCI.ProjectID)
		var blackListCves []Vulnerability
		mBlacklistCves := make(map[string]bool)
		for _, cve := range scaConfig.Cves {
			mBlacklistCves[cve] = true
		}
		// current cve project
		listCve := safebugClient.GetCvesProjectGitlab(this.GitlabCI.ProjectID)
		mCve := make(map[string]bool)
		for _, cve := range listCve {
			mCve[cve] = true
		}
		//
		mSeverity := make(map[string]int)
		for _, vuln := range rep.Vulnerabilities {
			if mBlacklistCves[vuln.Id] == true {
				blackListCves = append(blackListCves, vuln)
			}
			severity := safebug.ParseSeverity(vuln.Severity)
			// new cve
			if mCve[vuln.Id] != true {
				safebugClient.CreateCve(safebug.CreateCveRequest{
					Id:                vuln.Id,
					Dependency:        vuln.PackageName,
					DependencyVersion: vuln.PackageVersion,
					Description:       vuln.Description,
					Severity:          severity,
					Solution:          vuln.Status,
					Reference:         vuln.Link,
				})
			}
			mSeverity[severity] += 1
		}
		baseUrl := safebugClient.GetBaseURL()
		// black when has blacklist cve
		if len(blackListCves) > 0 {
			log.Error("Quality Gate Failed due ", len(blackListCves), " vulnerabilities in blacklist")
			tbl := table.New("CVE", "Severity", "Package", "Version")
			tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc()).
				WithFirstColumnFormatter(color.New(color.FgYellow).SprintfFunc())
			for _, cve := range blackListCves {
				tbl.AddRow(cve.Id, cve.Severity, cve.PackageName, cve.PackageVersion)
			}
			tbl.Print()
			log.Infof("Details here: %s/#/project/%s/cve", baseUrl, projectId)
			os.Exit(1)
		}
		// quality gate
		if scaConfig.Enable {
			pass := qualityGate(scaConfig, mSeverity)
			if pass {
				log.Info("Quality Gate: Success")
			} else {
				log.Error("Quality Gate: Failed")
				log.Infof("Details here: %s/#/project/%s/cve", baseUrl, projectId)
				os.Exit(1)
			}
		}

	} else {
		log.Warning("Safebug not set. Ignore")
	}

	// convert to gitlab-report
	mDependencies := make(map[string]report.DependencyFile)
	mFile := make(map[string]string)
	for _, pack := range rep.Packages {
		path := strings.ReplaceAll(pack.Path, prePath, "")
		path = strings.Trim(path, "/")
		dependencyFile, ok := mDependencies[path]
		if ok == false {
			dependencyFile = report.DependencyFile{
				Path:           path,
				PackageManager: "nexus",
				Dependencies:   []report.Dependency{},
			}
		}
		version := pack.Version
		if version == "" {
			version = "unknown"
		}
		dependencyFile.Dependencies = append(dependencyFile.Dependencies, report.Dependency{
			Package: report.Package{
				Name: strings.Replace(pack.Name, "_", "/", 1),
			},
			Version: version,
		})
		mFile[pack.Name+pack.Version] = path
		mDependencies[path] = dependencyFile
	}
	var issues []report.Vulnerability
	for _, vulnerability := range rep.Vulnerabilities {
		file := mFile[vulnerability.PackageName+vulnerability.PackageVersion]
		if file == "" {
			file = "unknown"
		}
		issue := report.Vulnerability{
			Name:        fmt.Sprintf("%s - %s version %s", vulnerability.Id, vulnerability.PackageName, vulnerability.PackageVersion),
			Category:    report.CategoryDependencyScanning,
			Scanner:     &scanner,
			Description: vulnerability.Description,
			Severity:    severityLevel(vulnerability.Severity),
			CompareKey:  vulnerability.Id,
			Links: []report.Link{
				{
					Name: vulnerability.Link,
					URL:  vulnerability.Link,
				},
			},
			Identifiers: []report.Identifier{
				ParseIdentifierID(vulnerability.Id),
			},
			Location: report.Location{
				Dependency: &report.Dependency{
					Package: report.Package{
						Name: vulnerability.PackageName,
					},
					Version: vulnerability.PackageVersion,
				},
				File: file,
			},
			Solution: vulnerability.Status,
		}
		issues = append(issues, issue)
	}
	newReport := report.NewReport()
	for _, dependencyFile := range mDependencies {
		newReport.DependencyFiles = append(newReport.DependencyFiles, dependencyFile)
	}
	newReport.Vulnerabilities = issues
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Version = report.CurrentVersion()
	newReport.Scan.Scanner = metadata.ReportScanner
	newReport.Scan.Analyzer = metadata.ReportScanner
	newReport.Scan.Type = report.CategoryDependencyScanning
	newReport.Scan.Status = report.StatusSuccess
	return &newReport, nil
}

func qualityGate(scaConfig safebug.ScaThresholdConfig, mSeverity map[string]int) bool {
	log.Info("Severity Vulnerabilities")
	tbl := table.New("CRITICAL", "HIGH", "MEDIUM", "LOW", "INFO")
	tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc())
	tbl.AddRow(mSeverity[safebug.Critical], mSeverity[safebug.High], mSeverity[safebug.Medium], mSeverity[safebug.Low], mSeverity[safebug.Info])
	tbl.Print()
	fmt.Println()
	log.Info("SCA Threshold Config")
	tbl = table.New("CRITICAL", "HIGH", "MEDIUM", "LOW")
	tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc())
	tbl.AddRow(scaConfig.Critical, scaConfig.High, scaConfig.Medium, scaConfig.Low)
	tbl.Print()
	if scaConfig.Critical > 0 && mSeverity[safebug.Critical] >= scaConfig.Critical {
		return false
	}
	if scaConfig.High > 0 && mSeverity[safebug.High] >= scaConfig.High {
		return false
	}
	if scaConfig.Medium > 0 && mSeverity[safebug.Medium] >= scaConfig.Medium {
		return false
	}
	if scaConfig.Low > 0 && mSeverity[safebug.Low] >= scaConfig.Low {
		return false
	}
	return true
}

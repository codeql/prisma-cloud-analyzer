package analyzer

import (
	"bufio"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
)

func fileExists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	}
	return false
}

func printStdout(stdout io.ReadCloser) {
	reader := bufio.NewReader(stdout)
	line, _, err := reader.ReadLine()
	for {
		if err != nil || line == nil {
			break
		}
		log.Println(string(line))
		line, _, err = reader.ReadLine()
	}
}

func printStderr(stdout io.ReadCloser) {
	reader := bufio.NewReader(stdout)
	line, _, err := reader.ReadLine()
	for {
		if err != nil || line == nil {
			break
		}
		log.Errorln(string(line))
		line, _, err = reader.ReadLine()
	}
}

package analyzer

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"time"
)

func ScaCommand() *cli.Command {
	return &cli.Command{
		Name:      "sca",
		Usage:     "Analyze sca project and generate report",
		ArgsUsage: "<project-dir>",
		Flags:     scaFlags(),
		Action: func(c *cli.Context) error {
			startTime := report.ScanTime(time.Now())
			// no args
			if c.Args().Present() {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errors.New("Invalid arguments")
			}
			// analyze
			log.Info("Running scanner")
			scanner := NewScaAnalyzer(c)
			reportFile, err := scanner.Analyze()
			if err != nil {
				return err
			}
			// convert
			log.Info("Creating report")
			newReport, err := scanner.Convert(reportFile)
			if err != nil {
				return err
			}
			endTime := report.ScanTime(time.Now())
			newReport.Scan.StartTime = &startTime
			newReport.Scan.EndTime = &endTime
			newReport.Sort()
			if err = SerializeJSONToFile(newReport, ArtifactNameDependencyScanning, "", false, false); err != nil {
				cerr := reportFile.Close()
				if cerr != nil {
					return cerr
				}
				return err
			}
			return reportFile.Close()
		},
	}
}

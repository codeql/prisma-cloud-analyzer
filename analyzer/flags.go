package analyzer

import (
	"github.com/urfave/cli/v2"
)

const (
	FlagTargetDir    = "target-dir"
	FlagTwistCliPath = "twistcli-path"
	FlagUrl          = "url"
	FlagUser         = "user"
	FlagPass         = "password"
	FlagOutput       = "output"
	FlagDetails      = "details"
	FlagDockerImage  = "docker-image"
	FlagTarImagePath = "tar-image-path"
	FlagImageName    = "image-name"
	FlagRepoName     = "repo-name"
	FlagRepoPath     = "repo-path"
	OutputFile       = "scan-results.json"
	flagSafebugUrl   = "safebug-url"
	flagSafebugToken = "safebug-token"
)

func globalFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    FlagTargetDir,
			Usage:   "Target directory",
			Value:   ".",
			EnvVars: []string{"CI_PROJECT_DIR"},
		},
		&cli.StringFlag{
			Name:    FlagTwistCliPath,
			Usage:   "Path to twistcli. Default is twistcli",
			Value:   "twistcli",
			EnvVars: []string{"TWISTCLI_PATH"},
		},
		&cli.StringFlag{
			Name:    FlagUrl,
			Usage:   "URL for Prisma Cloud Console. Example: https://us-west1.cloud.twistlock.com/us-3-123456789",
			EnvVars: []string{"PCC_URL"},
		},
		// ignore security issue here
		&cli.StringFlag{
			Name:    FlagUser,
			Aliases: []string{"u"},
			Usage:   "Access Key ID to access Prisma Cloud. If not provided, the PCC_USER environment variable is used",
			EnvVars: []string{"PCC_USER"},
		},
		&cli.StringFlag{
			Name:    FlagPass,
			Aliases: []string{"p"},
			Usage:   "Secret Key for the above Access Key ID specified with -u, --user. If not specified on the command-line, the PCC_PASSWORD environment variable is used",
			EnvVars: []string{"PCC_PASSWORD"},
		},
		&cli.StringFlag{
			Name:    FlagOutput,
			Aliases: []string{"o"},
			Value:   OutputFile,
			Usage:   "Write the results of the scan to a file in JSON format. Example: --output scan-results.json",
		},
		&cli.BoolFlag{
			Name:    FlagDetails,
			EnvVars: []string{"SHOW_DETAIL"},
			Value:   true,
			Usage:   "Show all vulnerability details. Can be specified with the SHOW_DETAIL",
		},
		&cli.StringFlag{
			Name:    flagSafebugUrl,
			EnvVars: []string{"SAFEBUG_URL"},
			Usage:   "SafeBug URL",
		},
		&cli.StringFlag{
			Name:    flagSafebugToken,
			EnvVars: []string{"SAFEBUG_TOKEN"},
			Usage:   "SafeBug Token",
		},
	}
}

func containerFlags() []cli.Flag {
	return append(globalFlags(),
		&cli.StringFlag{
			Name:    FlagTarImagePath,
			EnvVars: []string{"CI_PROJECT_PATH_SLUG"},
			Value:   "image.tar",
			Usage:   "Path to tar image to scan",
		},
		&cli.StringFlag{
			Name:    FlagImageName,
			EnvVars: []string{"CI_PROJECT_PATH"},
			Value:   "image",
			Usage:   "Image name",
		},
		&cli.StringFlag{
			Name:    FlagDockerImage,
			EnvVars: []string{"DOCKER_IMAGE"},
			Usage:   "docker image to scan",
		})
}

func scaFlags() []cli.Flag {
	return append(globalFlags(),
		&cli.StringFlag{
			Name:     FlagRepoName,
			EnvVars:  []string{"REPO_NAME", "CI_PROJECT_PATH"},
			Required: true,
			Usage:    "Repository name. Can be specified with the REPO_NAME or CI_PROJECT_PATH environment variable",
		},
		&cli.StringFlag{
			Name:    FlagRepoPath,
			EnvVars: []string{"REPO_PATH", "CI_PROJECT_DIR"},
			Value:   ".",
			Usage:   "Path to repository. Default is current dir",
		})
}

package analyzer

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"prisma-cloud-analyzer/metadata"
)

func NewContainerAnalyzer(c *cli.Context) Analyzer {
	return &ContainerAnalyzer{
		BaseAnalyzer: BaseAnalyzer{
			TargetDir:    c.String(FlagTargetDir),
			TwistCliPath: c.String(FlagTwistCliPath),
			ConsoleUrl:   c.String(FlagUrl),
			User:         c.String(FlagUser),
			Password:     c.String(FlagPass),
			OutputFile:   c.String(FlagOutput),
			ShowDetail:   c.Bool(FlagDetails),
			GitlabCI:     NewEnvGitlabCI(),
			SafebugURL:   c.String(flagSafebugUrl),
			SafebugToken: c.String(flagSafebugToken),
		},
		DockerImage:  c.String(FlagDockerImage),
		TarImagePath: c.String(FlagTarImagePath),
		ImageName:    c.String(FlagImageName),
	}
}

type ContainerAnalyzer struct {
	BaseAnalyzer
	DockerImage  string
	TarImagePath string
	ImageName    string
}

func (this *ContainerAnalyzer) Analyze() (io.ReadCloser, error) {
	args := []string{
		"images", "scan",
		"--address", this.ConsoleUrl,
		"--user", this.User,
		"--password", this.Password,
		"--output-file", this.OutputFile,
	}
	if this.ShowDetail {
		args = append(args, "--details")
	}
	dockerfile := filepath.Join(this.TargetDir, "Dockerfile")
	if this.DockerImage == "" && fileExists(dockerfile) == false {
		log.Panic("Dockerfile or docker image not found")
	}
	if this.DockerImage == "" {
		// build image
		if fileExists(this.TarImagePath) == false {
			this.BuildImage(dockerfile)
		}
		args = append(args, "--tarball", this.TarImagePath)
	} else {
		args = append(args, this.DockerImage)
	}
	cmd := exec.Command(this.TwistCliPath, args...)
	log.Info("Execute command:", cmd.String())
	cmd.Env = os.Environ()
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		log.Panic("Execute command scan image error:", err.Error())
	}
	go printStdout(stdout)
	go printStderr(stderr)
	_ = cmd.Wait()
	return os.Open(this.OutputFile)
}

func (this *ContainerAnalyzer) Convert(reader io.Reader) (*report.Report, error) {
	buf, err := io.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	var scanner = report.Scanner{
		ID:   metadata.AnalyzerID,
		Name: metadata.AnalyzerName,
	}
	var rep ContainerReport
	_ = json.Unmarshal(buf, &rep)
	var issues []report.Vulnerability
	for _, result := range rep.Results {
		for _, vulnerability := range result.Vulnerabilities {
			issue := report.Vulnerability{
				Name:        fmt.Sprintf("%s - %s version %s", vulnerability.Id, vulnerability.PackageName, vulnerability.PackageVersion),
				Title:       fmt.Sprintf("%s - %s version %s", vulnerability.Id, vulnerability.PackageName, vulnerability.PackageVersion),
				Category:    report.CategoryContainerScanning,
				Scanner:     &scanner,
				Description: vulnerability.Description,
				Severity:    severityLevel(vulnerability.Severity),
				CompareKey:  vulnerability.Id,
				Links: []report.Link{
					{
						Name: vulnerability.Link,
						URL:  vulnerability.Link,
					},
				},
				Identifiers: []report.Identifier{
					ParseIdentifierID(vulnerability.Id),
				},
				Location: report.Location{
					Dependency: &report.Dependency{
						Package: report.Package{
							Name: vulnerability.PackageName,
						},
						Version: vulnerability.PackageVersion,
					},
					OperatingSystem: result.Distro,
					Image:           result.Name,
				},
				Solution: vulnerability.Status,
			}
			issues = append(issues, issue)
		}
	}
	newReport := report.NewReport()
	newReport.Vulnerabilities = issues
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Version = report.CurrentVersion()
	newReport.Scan.Scanner = metadata.ReportScanner
	newReport.Scan.Analyzer = metadata.ReportScanner
	newReport.Scan.Type = report.CategoryContainerScanning
	newReport.Scan.Status = report.StatusSuccess
	return &newReport, nil
}

func (this *ContainerAnalyzer) BuildImage(dockerfile string) {
	log.Info("Build image")
	// kaniko/executor --context projectDir --dockerfile projectDir/Dockerfile --no-push  --destination=image --tarPath=projectDir/image.tar
	cmd := exec.Command("/kaniko/executor",
		"--no-push", "--single-snapshot",
		"--context", this.TargetDir,
		"--dockerfile", dockerfile,
		"--destination", this.ImageName, "--tarPath", this.TarImagePath)
	log.Info("Execute command:", cmd.String())
	cmd.Env = os.Environ()
	stderr, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		log.Panic("Build image error:", err.Error())
	}
	go printStdout(stderr)
	err = cmd.Wait()
	if err != nil {
		log.Panic("Build image error:", err.Error())
	}
	log.Info("Build image success")
}

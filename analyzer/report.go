package analyzer

import (
	"bytes"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"io"
	"os"
	"os/exec"
	"strings"
	"time"
)

const (
	ArtifactNameDependencyScanning = "gl-dependency-scanning-report.json"
	ArtifactNameContainerScanning  = "gl-container-scanning-report.json"
)

type Vulnerability struct {
	Id               string    `json:"id"`
	Status           string    `json:"status"`
	Cvss             float64   `json:"cvss,omitempty"`
	Vector           string    `json:"vector,omitempty"`
	Description      string    `json:"description"`
	Severity         string    `json:"severity"`
	PackageName      string    `json:"packageName"`
	PackageVersion   string    `json:"packageVersion"`
	Link             string    `json:"link"`
	RiskFactors      []string  `json:"riskFactors,omitempty"`
	ImpactedVersions []string  `json:"impactedVersions"`
	PublishedDate    time.Time `json:"publishedDate"`
	DiscoveredDate   time.Time `json:"discoveredDate"`
	FixDate          time.Time `json:"fixDate,omitempty"`
	LayerTime        time.Time `json:"layerTime"`
	LayerInstruction string    `json:"layerInstruction"`
	PackagePath      string    `json:"packagePath,omitempty"`
}

type SCAReport struct {
	Repository string `json:"repository"`
	Passed     bool   `json:"passed"`
	Packages   []struct {
		Type    string `json:"type"`
		Name    string `json:"name"`
		Version string `json:"version"`
		Path    string `json:"path"`
	} `json:"packages"`
	ComplianceIssues       interface{} `json:"complianceIssues"`
	ComplianceDistribution struct {
		Critical int `json:"critical"`
		High     int `json:"high"`
		Medium   int `json:"medium"`
		Low      int `json:"low"`
		Total    int `json:"total"`
	} `json:"complianceDistribution"`
	Vulnerabilities           []Vulnerability `json:"vulnerabilities"`
	VulnerabilityDistribution struct {
		Critical int `json:"critical"`
		High     int `json:"high"`
		Medium   int `json:"medium"`
		Low      int `json:"low"`
		Total    int `json:"total"`
	} `json:"vulnerabilityDistribution"`
}

type ContainerReport struct {
	Results []struct {
		Id                      string          `json:"id"`
		Name                    string          `json:"name"`
		Distro                  string          `json:"distro"`
		DistroRelease           string          `json:"distroRelease"`
		Collections             []string        `json:"collections"`
		ComplianceScanPassed    bool            `json:"complianceScanPassed"`
		Vulnerabilities         []Vulnerability `json:"vulnerabilities"`
		VulnerabilityScanPassed bool            `json:"vulnerabilityScanPassed"`
		ScanTime                time.Time       `json:"scanTime"`
		ScanID                  string          `json:"scanID"`
	} `json:"results"`
	ConsoleURL string `json:"consoleURL"`
}

// SerializerFunc function for serializing and optimizing report output
type SerializerFunc func(report *report.Report, artifactPath string, flagPrependPath string, indent bool, optimize bool) error

// SerializeJSONToFile will write a report to a path
func SerializeJSONToFile(report *report.Report, artifactPath string, flagPrependPath string, indent bool, optimize bool) error {
	/* #nosec */
	artifactFile, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	return SerializeJSONToWriter(report, artifactFile, flagPrependPath, indent, optimize)
}

// SerializeJSONToWriter optimizes the report based on the optimize flag
// and then encodes the report
func SerializeJSONToWriter(report *report.Report, writer io.Writer, flagPrependPath string, indent bool, optimize bool) error {
	outReport := report
	enc := json.NewEncoder(writer)
	if indent {
		enc.SetIndent("", "  ")
	}

	if optimize {
		var err error
		outReport, err = EliminateRedundancies(outReport, flagPrependPath)
		if err != nil {
			return err
		}
	}

	return enc.Encode(outReport)
}

// EliminateRedundancies is a function that helps to remove redundancies
// automatically from reports these optimizations include:
// - only include findings that refer to (git) tracked files
// - remove redundant LineEnd information
func EliminateRedundancies(notOptimized *report.Report, flagPrependPath string) (*report.Report, error) {
	log.Debug("Optimizing JSON Output")
	trackedFiles := make(map[string]struct{})

	gitPath, err := exec.LookPath("git")
	if err == nil {
		// get a list of all tracked files
		/* #nosec */
		cmd := exec.Command(gitPath, "ls-tree", "--full-tree", "-r", "--name-only", "HEAD")
		var out bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &stderr
		cmd.Dir = flagPrependPath

		err = cmd.Run()
		// fail gracefully if the git command fails
		if err == nil {
			for _, f := range strings.Split(out.String(), "\n") {
				// do not add empty string to the list of tracked files
				if f == "" {
					continue
				}
				trackedFiles[f] = struct{}{}
			}
		}
	} else {
		// proceed without filtering
		log.Warn("Could not detect git executable")
	}

	optimized := []report.Vulnerability{}

	for _, vuln := range notOptimized.Vulnerabilities {
		_, included := trackedFiles[vuln.Location.File]

		// do not add entries to the report for untracked files
		if len(trackedFiles) > 0 && !included {
			continue
		}

		linediff := vuln.Location.LineEnd - vuln.Location.LineStart
		// LineEnd == 0 happens if the LineEnd is undefined in the JSON file
		if linediff < 0 && vuln.Location.LineEnd != 0 {
			// LineEnd is defined and LineStart > LineEnd
			log.Fatalf("Invalid vulnerability location: LineStart > LineEnd")
		} else if linediff == 0 {
			// LineEnd == LineStart
			vuln.Location.LineEnd = 0
		}

		optimized = append(optimized, vuln)
	}

	rep := report.NewReport()
	rep.Version = notOptimized.Version
	rep.Vulnerabilities = optimized
	rep.Remediations = notOptimized.Remediations
	rep.DependencyFiles = notOptimized.DependencyFiles
	rep.Scan = notOptimized.Scan
	rep.Analyzer = notOptimized.Analyzer
	rep.Config = notOptimized.Config

	return &rep, nil
}

// severityLevel converts severity to a generic severity level.
func severityLevel(s string) report.SeverityLevel {
	s = strings.ToLower(s)
	switch s {
	case "critical":
		return report.SeverityLevelCritical
	case "high":
		return report.SeverityLevelHigh
	case "medium":
		return report.SeverityLevelMedium
	case "moderate":
		return report.SeverityLevelMedium
	case "low":
		return report.SeverityLevelLow
	case "info":
		return report.SeverityLevelInfo
	}
	return report.SeverityLevelUnknown
}

func ParseIdentifierID(idStr string) report.Identifier {
	identifer, success := report.ParseIdentifierID(idStr)
	if success == false {
		identifer.Type = "unknown"
		identifer.Name = idStr
		identifer.Value = idStr
	}
	return identifer
}

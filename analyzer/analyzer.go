package analyzer

import (
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"io"
	"os"
)

type Analyzer interface {
	Analyze() (io.ReadCloser, error)
	Convert(reader io.Reader) (*report.Report, error)
}

type BaseAnalyzer struct {
	TargetDir    string
	TwistCliPath string
	ConsoleUrl   string
	User         string
	Password     string
	OutputFile   string
	ShowDetail   bool
	GitlabCI     EnvGitlabCI
	SafebugURL   string
	SafebugToken string
}

type EnvGitlabCI struct {
	ProjectID     string
	ProjectURL    string
	ProjectPath   string
	DefaultBranch string
	CommitBranch  string
	PipelineID    string
	PipelineURL   string
	CommitSha     string
}

func NewEnvGitlabCI() EnvGitlabCI {
	return EnvGitlabCI{
		ProjectID:     os.Getenv("CI_PROJECT_ID"),
		ProjectURL:    os.Getenv("CI_PROJECT_URL"),
		ProjectPath:   os.Getenv("CI_PROJECT_PATH"),
		DefaultBranch: os.Getenv("CI_DEFAULT_BRANCH"),
		CommitBranch:  os.Getenv("CI_COMMIT_BRANCH"),
		PipelineID:    os.Getenv("CI_PIPELINE_ID"),
		PipelineURL:   os.Getenv("CI_PIPELINE_URL"),
		CommitSha:     os.Getenv("CI_COMMIT_SHA"),
	}
}

FROM golang:1.20-alpine AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN PATH_TO_MODULE=`go list -m`
RUN go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=30.00.140'" -o /analyzer

FROM docker:19.03
# add kaniko
COPY --from=gcr.io/kaniko-project/executor:v1.14.0-debug /kaniko/executor /kaniko/executor
RUN chmod +x /kaniko/executor
ENV SCANNER_VERSION=30.00.140
# add git curl zip
RUN apk update
RUN apk add git curl zip
# add twistcli
COPY twistcli.zip twistcli.zip
RUN unzip twistcli.zip -d /
RUN chmod +x /twistcli
ENV TWISTCLI_PATH=/twistcli
# add analyzer
COPY --from=build /analyzer /analyzer
RUN chmod +x /analyzer
ENTRYPOINT [""]

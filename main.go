package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"os"
	"prisma-cloud-analyzer/analyzer"
	"prisma-cloud-analyzer/metadata"
)

func main() {
	app := cli.NewApp()
	app.Name = metadata.AnalyzerName
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	if metadata.ReportScanner.Version == "" {
		metadata.ReportScanner.Version = "1.0"
	}
	app.Commands = []*cli.Command{
		analyzer.ContainerCommand(),
		analyzer.ScaCommand(),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

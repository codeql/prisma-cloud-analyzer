package plugin

import (
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
	"os"
	"prisma-cloud-analyzer/metadata"
)

func Match(path string, info os.FileInfo) (bool, error) {
	return true, nil
}

func init() {
	plugin.Register(metadata.AnalyzerID, Match)
}

package safebug

type AccessToken struct {
	Token string `json:"token"`
}

type CreateUpdateProjectGitlabRequest struct {
	Name             string `json:"name"`
	ProjectGitlabId  string `json:"projectGitlabId"`
	ProjectGitlabUrl string `json:"projectGitlabUrl"`
}

type DependencyProject struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Version  string `json:"version"`
	Location string `json:"location"`
}

type CreateDependencyProjectRequest struct {
	Name     string `json:"name"`
	Version  string `json:"version"`
	Location string `json:"location"`
}

type CreateCveRequest struct {
	Id                string `json:"id"`
	Dependency        string `json:"dependency"`
	DependencyVersion string `json:"dependencyVersion"`
	Description       string `json:"description"`
	Severity          string `json:"severity"`
	Solution          string `json:"solution"`
	Reference         string `json:"reference"`
}

type ScaThresholdConfig struct {
	Enable   bool     `json:"enable"`
	Critical int      `json:"critical"`
	High     int      `json:"high"`
	Medium   int      `json:"medium"`
	Low      int      `json:"low"`
	Cves     []string `json:"cves"`
}

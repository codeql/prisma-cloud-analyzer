package safebug

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"net/url"
)

type SafeBug struct {
	Token       string
	AccessToken string
	BaseURL     string
	client      *resty.Client
}

func NewSafeBug(baseUrl string, token string) *SafeBug {
	return &SafeBug{
		Token:   token,
		BaseURL: baseUrl,
		client:  resty.New(),
	}
}

func (safebug *SafeBug) RefreshToken() {
	api, _ := url.JoinPath(safebug.BaseURL, "api/refresh-token")
	var token AccessToken
	resp, err := safebug.client.R().
		SetBody(map[string]string{"token": safebug.Token}).
		SetResult(&token).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Panic("Unauthorized: SAFEBUG_TOKEN invalid")
	}
	safebug.AccessToken = token.Token
}

func (safebug *SafeBug) CreateOrUpdateProjectGitlab(body CreateUpdateProjectGitlabRequest) string {
	api, _ := url.JoinPath(safebug.BaseURL, "/api/gitlab-ci/project")
	var result string
	resp, err := safebug.request().
		SetBody(body).
		SetResult(&result).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Error(resp.StatusCode())
		logrus.Error(resp.Result())
		logrus.Panic(resp.Error())
	}
	return result
}

func (safebug *SafeBug) GetCvesProjectGitlab(projectGitlabId string) []string {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/cve", projectGitlabId))
	var result []string
	resp, err := safebug.request().
		SetResult(&result).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() == 200 {
		return result
	}
	logrus.Error(resp.StatusCode())
	logrus.Error(resp.Result())
	return nil
}

func (safebug *SafeBug) GetDependenciesProjectGitlab(projectGitlabId string) []DependencyProject {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/dependency", projectGitlabId))
	var result []DependencyProject
	resp, err := safebug.request().
		SetResult(&result).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() == 200 {
		return result
	}
	logrus.Error(resp.StatusCode())
	logrus.Error(resp.Result())
	return nil
}

func (safebug *SafeBug) CreateDependencyProjectGitlab(projectGitlabId string, body CreateDependencyProjectRequest) {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/dependency", projectGitlabId))
	resp, err := safebug.request().
		SetBody(body).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() == 200 {
		return
	}
	logrus.Error(resp.StatusCode())
	logrus.Error(resp.Result())
}

func (safebug *SafeBug) DeleteDependencyProjectGitlab(projectGitlabId string, dependencyId int) {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/dependency/%d", projectGitlabId, dependencyId))
	resp, err := safebug.request().
		Delete(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() == 200 {
		return
	}
	logrus.Error(resp.StatusCode())
	logrus.Error(resp.Result())
}

func (safebug *SafeBug) CreateCve(body CreateCveRequest) {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/cve"))
	resp, err := safebug.request().
		SetBody(body).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() == 200 {
		return
	}
	logrus.Error(resp.StatusCode())
	logrus.Error(resp.Result())
}

func (safebug *SafeBug) GetBaseURL() string {
	api, _ := url.JoinPath(safebug.BaseURL, "api/config/base-url")
	var baseUrl string
	_, err := safebug.request().
		SetBody(&baseUrl).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	return baseUrl
}

func (safebug *SafeBug) GetScaThresholdConfig(projectGitlabId string) ScaThresholdConfig {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/sca/threshold", projectGitlabId))
	var result ScaThresholdConfig
	resp, err := safebug.request().
		SetResult(&result).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() == 200 {
		return result
	}
	logrus.Error(resp.StatusCode())
	logrus.Error(resp.Result())
	return result
}

func (safebug *SafeBug) request() *resty.Request {
	return safebug.client.R().SetHeader("Authorization", "Bearer "+safebug.AccessToken)
}

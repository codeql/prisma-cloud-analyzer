package safebug

const (
	Critical = "Critical"
	High     = "High"
	Medium   = "Medium"
	Low      = "Low"
	Info     = "Info"
)

func ParseSeverity(severity string) string {
	if severity == "critical" {
		return Critical
	}
	if severity == "high" {
		return High
	}
	if severity == "medium" || severity == "moderate" {
		return Medium
	}
	if severity == "low" {
		return Low
	}
	return Info
}

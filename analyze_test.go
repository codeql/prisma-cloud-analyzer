package main

import (
	"os"
	"prisma-cloud-analyzer/analyzer"
	"testing"
)

const Token = "f5e334f3-797f-4473-8d95-e5b71d1341f4-fb0b41db-782e-4225-a4ba-f681e0bc2ef2"
const SafeBugUrl = "http://localhost:8000"

func TestAnalyzer(t *testing.T) {
	scanner := analyzer.ScaAnalyzer{
		BaseAnalyzer: analyzer.BaseAnalyzer{
			TargetDir:    "/builds/security-it-vps/testfork/demo-ci",
			TwistCliPath: "",
			ConsoleUrl:   "",
			User:         "",
			Password:     "",
			OutputFile:   "",
			ShowDetail:   false,
			GitlabCI: analyzer.EnvGitlabCI{
				ProjectID:     "50471840",
				ProjectURL:    "https://gitlab.com/test/example",
				ProjectPath:   "test/example",
				DefaultBranch: "main",
				CommitBranch:  "main",
				PipelineID:    "1",
				PipelineURL:   "https://gitlab.com/test/example/-/pipelines/1",
			},
			SafebugURL:   SafeBugUrl,
			SafebugToken: Token,
		},
		RepoName: "",
	}
	reader, _ := os.Open("test/scan-result.json")
	scanner.Convert(reader)
}
